package ru.malakhov.tm.repository;

import ru.malakhov.tm.api.repository.ICommandRepository;
import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.command.auth.LoginCommand;
import ru.malakhov.tm.command.auth.LogoutCommand;
import ru.malakhov.tm.command.project.*;
import ru.malakhov.tm.command.system.*;
import ru.malakhov.tm.command.task.*;
import ru.malakhov.tm.command.user.*;

import java.util.ArrayList;
import java.util.List;

public final class CommandRepository implements ICommandRepository {

    private static final Class[] COMMANDS = new Class[]{
            LoginCommand.class, LogoutCommand.class,

            ProjectClearCommand.class, ProjectCreateCommand.class, ProjectDisplayByIdCommand.class,
            ProjectDisplayByIndexCommand.class, ProjectDisplayByNameCommand.class, ProjectDisplayListCommand.class,
            ProjectRemoveByIdCommand.class, ProjectRemoveByIndexCommand.class, ProjectRemoveByNameCommand.class,
            ProjectUpdateByIdCommand.class, ProjectUpdateByIndexCommand.class,

            AboutCommand.class, ArgumentsCommand.class, CommandsCommand.class, ExitCommand.class, HelpCommand.class,
            SystemInfoCommand.class, VersionCommand.class,

            TaskClearCommand.class, TaskCreateCommand.class, TaskDisplayByIdCommand.class,
            TaskDisplayByIndexCommand.class, TaskDisplayByNameCommand.class, TaskDisplayListCommand.class,
            TaskRemoveByIdCommand.class, TaskRemoveByIndexCommand.class, TaskRemoveByNameCommand.class,
            TaskUpdateByIdCommand.class, TaskUpdateByIndexCommand.class,

            UserChangeEmailCommand.class, UserChangeFirstNameCommand.class, UserChangeLastNameCommand.class,
            UserChangeLoginCommand.class, UserChangeMiddleNameCommand.class, UserChangePasswordCommand.class,
            UserCreateCommand.class, UserProfileCommand.class
    };

    private final List<AbstractCommand> commandList = new ArrayList<>();

    {
        for (final Class clazz: COMMANDS) {
            try {
                final Object commandInstance = clazz.newInstance();
                final AbstractCommand command = (AbstractCommand) commandInstance;
                commandList.add(command);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<AbstractCommand> getCommandList() {
        return commandList;
    }

}