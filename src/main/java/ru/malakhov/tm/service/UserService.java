package ru.malakhov.tm.service;

import ru.malakhov.tm.api.repository.IUserRepository;
import ru.malakhov.tm.api.service.IUserService;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.util.HashUtil;
import ru.malakhov.tm.exception.empty.*;
import ru.malakhov.tm.model.Role;

import java.util.List;

public final class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(final IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User add(final User user) {
        return userRepository.add(user);
    }

    @Override
    public User findById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(id);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public User removeById(final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(id);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User create(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User(login, HashUtil.salt(password));
        user.setRole(Role.USER);
        return userRepository.add(user);
    }

    @Override
    public User create(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        final User user = create(login,password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User create(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        final User user = create(login,password);
        if (user == null) return null;
        user.setRole(role);
        return null;
    }

    @Override
    public String[] profile(String id) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        return userRepository.profile(id);
    }

    @Override
    public User changeEmail(String id, final String email) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.changeEmail(id, email);
    }

    @Override
    public User changePassword(final String id, final String password) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        return userRepository.changePassword(id, password);
    }

    @Override
    public User changeLogin(final String id, final String login) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.changeLogin(id, login);
    }

    @Override
    public User changeFirstName(final String id, final String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return userRepository.changeFirstName(id, name);
    }

    @Override
    public User changeMiddleName(final String id, final String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return userRepository.changeMiddleName(id, name);
    }

    @Override
    public User changeLastName(final String id, final String name) {
        if (id == null || id.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return userRepository.changeLastName(id, name);
    }

}