package ru.malakhov.tm.api.service;

public interface IAuthService {

    String getUserId();

    void login(String login, String password);

    void logout();

}