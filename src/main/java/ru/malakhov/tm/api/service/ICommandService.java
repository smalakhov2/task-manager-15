package ru.malakhov.tm.api.service;

import ru.malakhov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getCommandList();

}