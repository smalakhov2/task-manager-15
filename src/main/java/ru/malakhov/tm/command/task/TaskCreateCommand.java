package ru.malakhov.tm.command.task;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.util.TerminalUtil;

public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.TASK_CREATE;
    }

    @Override
    public String description() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getTaskService().create(userId, name, description);
        System.out.println("[OK]");
    }

}