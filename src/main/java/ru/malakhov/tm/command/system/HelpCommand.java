package ru.malakhov.tm.command.system;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.ArgumentConst;
import ru.malakhov.tm.constant.TerminalConst;

import java.util.List;

public final class HelpCommand extends AbstractCommand {

    @Override
    public String argument() {
        return ArgumentConst.HELP;
    }

    @Override
    public String name() {
        return TerminalConst.HELP;
    }

    @Override
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final List<AbstractCommand> commands = serviceLocator.getCommandService().getCommandList();
        for (final AbstractCommand command: commands) {
            System.out.println(command.name() + " - " + command.description());
        }
        System.out.println("[OK]");
    }

}