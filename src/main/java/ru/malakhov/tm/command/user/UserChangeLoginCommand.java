package ru.malakhov.tm.command.user;

import ru.malakhov.tm.command.AbstractCommand;
import ru.malakhov.tm.constant.TerminalConst;
import ru.malakhov.tm.entity.User;
import ru.malakhov.tm.util.TerminalUtil;

public final class UserChangeLoginCommand extends AbstractCommand {

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String name() {
        return TerminalConst.CHANGE_LOGIN;
    }

    @Override
    public String description() {
        return "Change login.";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CHANGE LOGIN]");
        System.out.println("ENTER NEW LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().changeLogin(userId, login);
        if (user == null) System.out.println("[FAIL]");
        else System.out.println("[OK]");
    }

}